NODE_ENV = 'production'

# just a flag
ENV = 'test'

# base api
VUE_APP_ACCOUNT_BASE_URL = 'https://bosstest.greatech.com.cn/account-api/'

VUE_APP_MANAGEMENT_BASE_URL = 'https://bosstest.greatech.com.cn/management-api/'
