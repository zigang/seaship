import permission from './modules/permission'
import account from './modules/account'
import department from './modules/department'
import role from './modules/role'
import user from './modules/user'
import manufacturer from './modules/manufacturer'
import deviceModel from './modules/deviceModel'
import device from './modules/device'
import log from './modules/log'
import service from './modules/service'
import repair from './modules/repair'
import archive from './modules/archive'
import health from './modules/health'
import disease from './modules/disease'
import news from './modules/news'
import file from './modules/file'
import alarm from './modules/alarm'

export default {
  permission,
  account,
  department,
  role,
  user,
  manufacturer,
  deviceModel,
  device,
  log,
  service,
  repair,
  archive,
  health,
  disease,
  news,
  file,
  alarm
}
