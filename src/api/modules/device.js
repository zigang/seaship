import request from '@/utils/request-management'

/**
 * 设备
 */
export default class Device {
  /**
   * 设备列表
   *
   * @param data
   * @returns {*}
   */
  static list = data => request.post('/api/manager/device/page', data)
  /**
   * 设备详情
   *
   * @param data
   * @returns {*}
   */
  static detail = data => request.get('/api/manager/device/detail/' + data.id)

  /**
   * 通过 家庭圈ID 查询设备列表
   *
   * @param data
   * @returns {*}
   */
  static upload = data => request.post('/api/manager/device/import', data, { 'Content-Type': 'multipart/form-data' })
  /**
   * 通过 家庭圈ID 查询设备列表
   *
   * @param data
   * @returns {*}
   */
  static listByCircleId = data => request.post('/api/manager/member/page', data)
  /**
   * 佩戴者信息
   *
   * @param data
   * @returns {*}
   */
  static wearerInfo = data => request.get('/api/manager/largescreen/memberinfo', data)
  /**
   * 获取设备位置信息
   *
   * @param data
   * @returns {*}
   */
  static locate = data => request.post('/api/manager/healthrecord/locationOrder/' + data.id)
}
