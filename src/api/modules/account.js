import request from '@/utils/request'

export default class Account {
  /**
   * 新增用户
   *
   * @param data
   * @returns {*}
   */
  static create = data => request.post('/api/auth/register', data)

  /**
   * 更新用户信息
   *
   * @param data
   * @returns {*}
   */
  static delete = data => request.post('/api/auth/del/' + data.id)

  /**
   * 更新用户信息
   *
   * @param data
   * @returns {*}
   */
  static modify = data => request.post('/api/auth/update', data)

  /**
   * 账号列表
   *
   * @param data
   * @returns {*}
   */
  static list = data => request.post('/api/auth/page', data)

  /**
   * 部门列表
   *
   * @param data
   * @returns {*}
   */
  static login = data => request.post('/api/auth/login', data)

  /**
   * 更新用户密码
   *
   * @param data
   * @returns {*}
   */
  static modifyPassword = data => request.post('/api/auth/revise', data)

  /**
   * 获取验证码 - 修改密码
   *
   * @param data
   * @returns {*}
   */
  static getVerifyCode = data => request.post('/api/auth/revisecode', data)

  /**
   * 菜单
   *
   * @param data
   * @returns {*}
   */
  static permissions = data => request.get('/api/manager/auth/userauth/' + data.id)

  /**
   * 获取用户信息
   *
   * @returns {*}
   */
  static getInfo = () => request.get('/api/auth/info')

  /**
   * 退出登录
   *
   * @param data
   * @returns {*}
   */
  // static logout = data => request.post('/home-for-old-age/user/logout', data)
}
