import request from '@/utils/request-management'

/**
 * 设备维修
 */
export default class Repair {
  /**
   * 设备维修记录列表
   *
   * @param data
   * @returns {*}
   */
  static list = data => request.post('/api/manager/device/repair/page', data)
  /**
   * 新增设备维修记录
   *
   * @param data
   * @returns {*}
   */
  static create = data => request.post('/api/manager/device/repair/add', data)
  /**
   * 更新设备维修记录
   *
   * @param data
   * @returns {*}
   */
  static modify = data => request.post('/api/manager/device/repair/update', data)
  /**
   * 更新设备维修记录
   *
   * @param data
   * @returns {*}
   */
  static delete = data => request.delete('/api/manager/device/repair/del/' + data.id)
}
