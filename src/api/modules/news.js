import request from '@/utils/request-management'

/**
 * 健康咨询
 */
export default class News {
  /**
   * 列表
   *
   * @returns {*}
   */
  static list = data => request.post('/api/manager/info/page', data)
  /**
   * 新增健康资讯
   *
   * @returns {*}
   */
  static create = data => request.post('/api/manager/info/add', data)
  /**
   * 新增健康资讯
   *
   * @returns {*}
   */
  static modify = data => request.post('/api/manager/info/update', data)
  /**
   * 删除健康资讯
   *
   * @returns {*}
   */
  static delete = data => request.delete('/api/manager/info/del/' + data.id)
}
