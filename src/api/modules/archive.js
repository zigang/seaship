import request from '@/utils/request-management'

/**
 * 档案
 */
export default class Archive {
  /**
   * 档案列表
   *
   * @param data 账号ID
   * @returns {*}
   */
  static listByAccountId = data => request.post('/api/manager/healtharchive/' + data.id)
}
