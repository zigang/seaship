import request from '@/utils/request-management'

/**
 * 文件
 */
export default class File {
  /**
   * 获取七牛 token
   *
   * @param fileName {String}
   * @returns {*}
   */
  static getQiNiuToken = data => request.get('/qiniu/getToken', data)
}
