import request from '@/utils/request-management'

/**
 * 日志
 */
export default class Log {
  /**
   * 日志列表
   *
   * @param data
   * @returns {*}
   */
  static list = data => request.post('/api/manager/sys/log/list', data)

}
