import request from '@/utils/request-management'

export default class User {
  /**
   * 用户列表
   *
   * @param data
   * @returns {*}
   */
  static list = data => request.post('/api/manager/user/page', data)

  /**
   * 用户详情
   *
   * @param data
   * @returns {*}
   */
  static detail = data => request.get('/api/manager/user/detail/' + data.id)
}
