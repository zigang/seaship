import request from '@/utils/request'

export default class Department {
  /**
   * 新增部门
   * @param data
   * @returns {*}
   */
  static create = data => request.post('/api/manager/dept/add', data)

  /**
   * 删除部门
   * @param data
   * @returns {*}
   */
  static delete = data => request.post('/api/manager/dept/del', data)

  /**
   * 修改部门信息
   * @param data
   * @returns {*}
   */
  static modify = data => request.post('/api/manager/dept/update', data)

  /**
   * 部门列表
   * @param data
   * @returns {*}
   */
  static list = data => request.post('/api/manager/dept/page', data)
}
