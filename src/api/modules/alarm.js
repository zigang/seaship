import request from '@/utils/request-management'

/**
 * 健康数据 - 报警
 */
export default class Alarm {
  /**
   * 健康报警
   *
   * @param data
   * @returns {*}
   */
  static healthList = data => request.post('/api/manager/warning/health/page', data)
  /**
   * 跌倒报警
   *
   * @param data
   * @returns {*}
   */
  static fallList = data => request.post('/api/manager/warning/fall/page', data)
  /**
   * SOS 报警
   *
   * @param data
   * @returns {*}
   */
  static sosList = data => request.post('/api/manager/warning/sos/page', data)
  /**
   * 围栏报警
   *
   * @param data
   * @returns {*}
   */
  static fenceList = data => request.post('/api/manager/warning/fence/page', data)
  /**
   * 预警处理--新增
   *
   * @param data
   * @returns {*}
   */
  static recordSave = data => request.post('/api/manager/warning/record/save', data)
   /**
   * 预警处理--查询
   *
   * @param data
   * @returns {*}
   */
  static recordDetail = data => request.get('/api/manager/warning/dealRecordDetail/' + data.warnId)
   /**
   * 预警统计--今日健康统计
   *
   * @param data
   * @returns {*}
   */
  static healthToday = data => request.post('/api/manager/warning/health/todayPage', data)
  /**
   * 预警统计--今日SOS统计
   *
   * @param data
   * @returns {*}
   */
  static sosToday = data => request.post('/api/manager/warning/sos/todaySosPage', data)
  /**
   * 预警统计--今日围栏统计
   *
   * @param data
   * @returns {*}
   */
  static fenceToday = data => request.post('/api/manager/warning/fence/todayFencePage', data)
   /**
   * 预警统计--总统计
   *
   * @param data
   * @returns {*}
   */
  static monitorTot = data => request.get('/api/manager/warning/health/monitorDataTot', data)
   /**
   * 预警统计--佩戴者地图信息
   *
   * @param data
   * @returns {*}
   */
  static wearerLocationInfo = data => request.get('/api/manager/warning/health/wearerLocationInfo', data)
  /**
   * 穿戴者数据统计 -- 图表
   *
   * @param data
   * @returns {*}
   */
  static wearerTot = data => request.get('/api/manager/warning/health/wearerTot', data)
  /**
   * 推送 -- 列表
   *
   * @param data
   * @returns {*}
   */
  static pushPage = data => request.post('/api/manager/pushnumber/page', data)
  /**
   * 推送 -- 添加
   *
   * @param data
   * @returns {*}
   */
  static pushAdd = data => request.post('/api/manager/pushnumber/add', data)
  /**
   * 推送 -- 修改
   *
   * @param data
   * @returns {*}
   */
  static pushUpdate = data => request.put('/api/manager/pushnumber/update', data)
  /**
   * 推送 -- 删除
   *
   * @param data
   * @returns {*}
   */
  static pushDelete = data => request.delete('/api/manager/pushnumber/delete/'+ data.id)
}
