import request from '@/utils/request-management'

/**
 * 设备型号
 */
export default class DeviceModel {
  /**
   * 设备型号列表
   *
   * @param data
   * @returns {*}
   */
  static list = data => request.post('/api/manager/devicemodel/page', data)
  /**
   * 新增设备型号
   *
   * @param data
   * @returns {*}
   */
  static create = data => request.post('/api/manager/devicemodel/add', data)
  /**
   * 修改设备型号
   *
   * @param data
   * @returns {*}
   */
  static modify = data => request.post('/api/manager/devicemodel/update', data)
  /**
   * 删除设备型号
   *
   * @param data
   * @returns {*}
   */
  static delete = data => request.delete('/api/manager/devicemodel/del/' + data.id)
  /**
   * 设备型号功能菜单
   *
   * @param data
   * @returns {*}
   */
  static menusById = data => request.get('/api/manager/appmenu/assignmenu/' + data.id)
}
