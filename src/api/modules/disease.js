import request from '@/utils/request-management'

/**
 * 疾病
 */
export default class Disease {
  /**
   * 列表
   *
   * @param type {string} 疾病类型(0：疾病数据 1：家族史 2：手术史)
   * @returns {*}
   */
  static list = data => request.post('/api/manager/disease/page', data)
  /**
   * 新增
   *
   * @param type {string} 疾病类型(0：疾病数据 1：家族史 2：手术史)
   * @param diseaseName {string} 疾病名称
   * @returns {*}
   */
  static create = data => request.post('/api/manager/disease/add', data)
  /**
   * 更新
   *
   * @param type {string} 疾病类型(0：疾病数据 1：家族史 2：手术史)
   * @param diseaseName {string} 疾病名称
   * @returns {*}
   */
  static modify = data => request.post('/api/manager/disease/update', data)
  /**
   * 更新
   *
   * @param id {long} 疾病ID
   * @returns {*}
   */
  static delete = data => request.delete('/api/manager/disease/del/' + data.id)
}
