import request from '@/utils/request'

/**
 * 角色
 */
export default class Role {
  /**
   * 创建角色
   *
   * @param data
   * @returns {*}
   */
  static create = data => request.post('/api/manager/role/add', data)
  /**
   * 删除角色
   *
   * @param data
   * @returns {*}
   */
  static delete = data => request.post('/api/manager/role/del', data)
  /**
   * 更新角色
   *
   * @param data
   * @returns {*}
   */
  static modify = data => request.post('/api/manager/role/update', data)
  /**
   * 角色列表
   *
   * @param data
   * @returns {*}
   */
  static list = data => request.post('/api/manager/role/page', data)
  /**
   * 角色列表
   *
   * @param data
   * @returns {*}
   */
  static listDetails = data => request.post('/api/manager/role/roleaccountpage', data)
  /**
   * 角色权限
   *
   * @param id 角色ID
   *
   * @returns {*}
   */
  static permissions = data => request.post('/api/manager/role/roleauth', data)
}
