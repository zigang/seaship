import request from '@/utils/request-management'

/**
 * 健康数据
 */
export default class Health {
  /**
   * 最新数据
   *
   * @param data
   * @returns {*}
   */
  static latest = data => request.get('/api/manager/largescreen/latestdata', data)
  /**
   * 设备列表
   *
   * @param data
   * @returns {*}
   */
  static list = data => request.post('/api/manager/healthrecord/page', data)
  /**
   * 定位轨迹
   *
   * @param data
   * @returns {*}
   */
  static activityTrackList = data => request.post('/api/manager/healthrecord/location', data)
  /**
   * 计步统计
   *
   * @param data
   * @returns {*}
   */
  static stepStat = data => request.post('/api/manager/healthrecord/steps/aggregatestatlist', data)
  /**
   * 计步汇总
   *
   * @param data
   * @returns {*}
   */
  static stepSummary = data => request.post('/api/manager/healthrecord/steps/aggregatestat', data)
  /**
   * 心率记录
   *
   * @param data
   * @returns {*}
   */
  static heartRateList = data => request.post('/api/manager/healthrecord/heartrate', data)
  /**
   * 心率统计
   *
   * @param data
   * @returns {*}
   */
  static heartRateStat = data => request.post('/api/manager/healthrecord/heartrate/aggregatestatlist', data)
  /**
   * 心率汇总
   *
   * @param data
   * @returns {*}
   */
  static heartRateSummary = data => request.post('/api/manager/healthrecord/heartrate/aggregatestat', data)
  /**
   * 血压记录
   *
   * @param data
   * @returns {*}
   */
  static bloodPressureList = data => request.post('/api/manager/healthrecord/bloodpressure', data)
  /**
   * 血压统计
   *
   * @param data
   * @returns {*}
   */
  static bloodPressureStat = data => request.post('/api/manager/healthrecord/bloodpressure/aggregatestatlist', data)
  /**
   * 血压汇总
   *
   * @param data
   * @returns {*}
   */
  static bloodPressureSummary = data => request.post('/api/manager/healthrecord/bloodpressure/aggregatestat', data)
  /**
   * 血氧记录
   *
   * @param data
   * @returns {*}
   */
  static bloodOxygenList = data => request.post('/api/manager/healthrecord/bloodoxygen', data)
  /**
   * 血氧统计
   *
   * @param data
   * @returns {*}
   */
  static bloodOxygenStat = data => request.post('/api/manager/healthrecord/bloodoxygen/aggregatestatlist', data)
  /**
   * 血氧汇总
   *
   * @param data
   * @returns {*}
   */
  static bloodOxygenSummary = data => request.post('/api/manager/healthrecord/bloodoxygen/aggregatestat', data)
  /**
   * 体温记录
   *
   * @param data
   * @returns {*}
   */
   static temperatureList = data => request.post('/api/manager/healthrecord/temperature', data)
   /**
    * 体温统计
    *
    * @param data
    * @returns {*}
    */
   static temperatureStat = data => request.post('/api/manager/healthrecord/temperature/aggregatestatlist', data)
   /**
    * 体温汇总
    *
    * @param data
    * @returns {*}
    */
   static temperatureSummary = data => request.post('/api/manager/healthrecord/temperature/aggregatestat', data)
  /**
   * 睡眠记录
   *
   * @param data
   * @returns {*}
   */
  static sleepList = data => request.post('/api/manager/healthrecord/sleep', data)
  /**
   * 睡眠统计
   *
   * @param data
   * @returns {*}
   */
  static sleepStat = data => request.post('/api/manager/healthrecord/sleep/aggregatestatlist', data)
  /**
   * 睡眠汇总
   *
   * @param data
   * @returns {*}
   */
  static sleepSummary = data => request.post('/api/manager/healthrecord/sleep/aggregatestat', data)
}
