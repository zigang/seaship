import request from '@/utils/request-management'

/**
 * 厂商
 */
export default class Manufacturer {
  /**
   * 厂商列表
   *
   * @param data
   * @returns {*}
   */
  static list = data => request.get('/api/manager/manufacturer/page', data)

  /**
   * 更新厂商信息
   *
   * @param data
   * @returns {*}
   */
  static modify = data => request.post('/api/manager/manufacturer/update', data)
}
