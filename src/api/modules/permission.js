import request from '@/utils/request'

/**
 * 权限
 */
export default class Permission {
  /**
   * 菜单
   *
   * @param data
   * @returns {*}
   */
  static menu = data => request.get('/api/manager/auth/userauth/' + data.id)
}
