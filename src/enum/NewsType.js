export default class NewsType {
  static ARTICLE = 0 // 文章
  static VIDEO = 1 // 视频

  static titleOf(value) {
    if (value === this.VIDEO) {
      return '视频资讯'
    }
    return '图文资讯'
  }
}
