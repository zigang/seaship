export default class ReleaseType {
  static ALL = 0 // 全部
  static APP = 1 // APP
  static APPLET = 2 // 小程序
  static valueOf(value) {
    if (value === this.APP) {
      return 'APP'
    }
    if (value === this.APPLET) {
      return '小程序'
    }
    return '全部'
  }
}
