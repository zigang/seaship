export default class NewsState {
  static DRAFT = 0 // 草稿
  static PUBLISHED = 1 // 已发布

  static valueOf(value) {
    if (value === this.PUBLISHED) {
      return '已发布'
    }
    return '草稿'
  }
}
