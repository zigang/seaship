export default class AlarmType {
  static BLOOD_PRESSURE = 0
  static HEART_RATE = 1
  static BLOOD_OXYGEN = 2
  static TEMPERATURE = 3

  static valueOf(value) {
    if (value === this.BLOOD_PRESSURE) {
      return '血压异常'
    } else if (value === this.HEART_RATE) {
      return '心率异常'
    } else if (value === this.BLOOD_OXYGEN) {
      return '血氧异常'
    } else if (value === this.TEMPERATURE) {
      return '体温异常'
    }
    return ''
  }

  static unitOf(value) {
    if (value === this.BLOOD_PRESSURE) {
      return 'mmHg'
    } else if (value === this.HEART_RATE) {
      return '次/分'
    } else if (value === this.BLOOD_OXYGEN) {
      return '%'
    } else if (value === this.TEMPERATURE) {
      return '℃'
    }
    return ''
  }
}
