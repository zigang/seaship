export default class StatType {
  static DAY = 1
  static MONTH = 2
  static YEAR = 3

  static dateTypeOf(dateType) {
    if (dateType === 'date') {
      return this.DAY
    } else if (dateType === 'month') {
      return this.MONTH
    } else if (dateType === 'year') {
      return this.YEAR
    }
    return this.DAY
  }
}
