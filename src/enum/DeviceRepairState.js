export default class DeviceRepairState {
  static REPAIRED = 0
  static RECYCLED = 1
  static RETURN_FACTORY = 2
  static REPLACED = 3
  static RETURNED = 4

  static valueOf(value) {
    if (value === this.REPAIRED) {
      return '已报修'
    } else if (value === this.RECYCLED) {
      return '已回收'
    } else if (value === this.RETURN_FACTORY) {
      return '返厂'
    } else if (value === this.REPLACED) {
      return '已更换'
    } else if (value === this.RETURNED) {
      return '已退货'
    }
    return ''
  }
}
