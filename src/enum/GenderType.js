export default class GenderType {
  static FEMALE = 0
  static MALE = 1

  static valueOf(value) {
    if (value === this.MALE) {
      return '男'
    } else if (value === this.FEMALE) {
      return '女'
    }
    return ''
  }
}
