export default class DiseaseType {
  static DISEASE = 0
  static FAMILY = 1
  static SURGERY = 2

  static valueOf(value) {
    if (value === this.DISEASE) {
      return '疾病史'
    } else if (value === this.FAMILY) {
      return '家族史'
    } else if (value === this.SURGERY) {
      return '手术史'
    }
    return ''
  }
}
