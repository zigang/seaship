import { asyncRoutes, constantRoutes } from '@/router'

/**
 * Use meta.role to determine if the current user has permission
 * @param permissions
 * @param route
 */
function hasPermission(permissions, route) {
  if (route.meta && route.meta.code) {
    return permissions.some(permission => route.meta.code === permission)
  } else {
    return true
  }
}

function flatRemoteRoutes(data) {
  let result = []
  if (!data || data.length === 0) {
    return result
  }
  for (const item of data) {
    result.push(item.rightCode)
    if (item.child && item.child.length > 0) {
      const subResult = flatRemoteRoutes(item.child)
      if (subResult.length > 0) {
        result = result.concat(subResult)
      }
    }
  }
  return result
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param permissions
 */
function filterAsyncRoutes(routes, permissions) {
  const result = []

  routes.forEach(route => {
    const tmp = { ...route }
    if (hasPermission(permissions, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, permissions)
      }
      result.push(tmp)
    }
  })

  return result
}

/**
 * 重定向修复
 *
 * @param routes
 * @returns {[]}
 */
function redirectRoutes(routes) {
  const result = []

  routes.forEach(route => {
    if (route.redirect && route.children && route.children.length > 0) {
      route.redirect = route.path + '/' + getFirstShowingChild(route.children)
    }

    result.push(route)
  })

  return result
}

function getFirstShowingChild(children = []) {
  if (!children || children.length === 0) {
    return null
  }
  const showingChildren = children.filter(item => !item.hidden)
  if (showingChildren.length >= 1) {
    return showingChildren[0].path
  }
  return null
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  generateRoutes({ commit }, remoteRoutes) {
    return new Promise(resolve => {
      const permissions = flatRemoteRoutes(remoteRoutes)
      const accessedRoutes = filterAsyncRoutes(asyncRoutes, permissions)
      const redirectedRoutes = redirectRoutes(accessedRoutes)
      commit('SET_ROUTES', redirectedRoutes)
      resolve(accessedRoutes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
