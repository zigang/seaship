import api from '@/api/index'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    id: 0, info: null, token: getToken(), name: '', avatar: '', permissions: []
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  }, SET_USER: (state, user) => {
    state.info = user
  }, SET_TOKEN: (state, token) => {
    state.token = token
  }, SET_ID: (state, id) => {
    state.id = id
  }, SET_NAME: (state, name) => {
    state.name = name
  }, SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  }, SET_PERMISSIONS: (state, permissions) => {
    state.permissions = permissions
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      api.account.login({ account: username.trim(), password: password }).then(response => {
        const { data } = response
        commit('SET_USER', data)
        commit('SET_TOKEN', data.token)
        setToken(data.token)

        const { id, avatar } = data

        commit('SET_ID', id)
        commit('SET_NAME', data.alias ? data.alias : data.account)
        commit('SET_AVATAR', avatar)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  getPermissions({ commit, state }, accountId) {
    return new Promise((resolve, reject) => {
      api.account.permissions({ id: accountId }).then(res => {
        const permissions = res.data.child

        if (!permissions || permissions.length === 0) {
          return reject('Verification failed, please Login again.')
        }

        commit('SET_PERMISSIONS', permissions)
        resolve(permissions)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit }) {
    return new Promise((resolve, reject) => {
      api.account.getInfo().then(response => {
        const { data } = response

        if (!data) {
          return reject('Verification failed, please Login again.')
        }

        commit('SET_USER', data)

        const { id, avatar } = data

        commit('SET_ID', id)
        commit('SET_NAME', data.alias ? data.alias : data.account)
        commit('SET_AVATAR', avatar)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      // api.account.logout(state.token).then(() => {
      removeToken() // must remove  token  first
      resetRouter()
      commit('RESET_STATE')
      resolve()
      // }).catch(error => {
      //   reject(error)
      // })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true, state, mutations, actions
}

