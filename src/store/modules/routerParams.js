
const state = {
  routerParams: {}
}

const mutations = {
  CHANGE_ROUTER_PARAMS: (state, params) => {
    state.routerParams = params
  }
}

export default {
  namespaced: true,
  state,
  mutations
}
