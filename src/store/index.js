import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import permission from './modules/perssion'
import routerParams from './modules/routerParams'

import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user,
    permission,
    routerParams
  },
  getters,
  plugins: [createPersistedState({
    key:'old-age-web-vuex',
    storage: window.sessionStorage,
    paths:['routerParams.routerParams']
    // reducer(val) {
    //    console.log('val',val)
    //      return {
    //          // 只储存state中的routerParams
    //          routerParams: val.routerParams.routerParams
    //      }
    //  }
 })]
})

export default store
