import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

const originalPush = Router.prototype.push

// 捕获异常 - 跳转同一路由报错异常
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  // index page must be placed at the first !!!
  {
    path: '/',
    component: Layout,
    redirect: '/index',
    children: [
      {
        path: 'index',
        name: 'Index',
        component: () => import('@/views/home/index'),
        hidden: true,
        meta: { title: '', icon: '' }
      }
    ]
  },

  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },

  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  }
]

export const asyncRoutes = [
  {
    path: '/account',
    component: Layout,
    redirect: '/account/permission',
    meta: { title: '账号管理', icon: 'iconfont icon-houtaiguanli-quanxianguanli', code: 'accountmanager' },
    children: [
      {
        path: 'permission',
        component: () => import('@/views/account/permission/index'),
        name: 'Permission',
        meta: { title: '权限管理', icon: 'iconfont icon--quanxianguanli', code: 'accountmanager_auth' }
      },
      {
        path: 'user',
        name: 'User',
        component: () => import('@/views/account/user/index'),
        meta: { title: '用户管理', icon: 'iconfont icon-yonghuguanli', code: 'accountmanager_user' }
      },
      {
        path: 'user/info',
        component: () => import('@/views/account/user/info/index'),
        name: 'Account Info',
        hidden: true,
        meta: { title: '用户详情', icon: 'dashboard', noCache: true, activeMenu: '/account/user', code: 'accountmanager_user' }
      }
    ]
  },
  {
    path: '/device',
    component: Layout,
    redirect: '/device/manage',
    meta: { title: '设备管理', icon: 'iconfont icon-zhinengshoubiao', code: 'devicemanager' },
    children: [
      {
        path: 'config',
        component: () => import('@/views/device/config/index'),
        name: 'Device Configuration',
        meta: { title: '设备配置', icon: 'iconfont icon-jiankangguanli', code: 'devicemanager_config' }
      },
      {
        path: 'list',
        component: () => import('@/views/device/list/index'),
        name: 'Device List',
        meta: { title: '设备信息', icon: 'iconfont icon-xinxi', code: 'devicemanager_list' }
      },
      {
        path: 'info',
        component: () => import('@/views/device/list/info/index'),
        name: 'Device Info',
        hidden: true,
        meta: { title: '设备详情', icon: 'home', noCache: true, activeMenu: '/device/list' }
      }
    ]
  },
  {
    path: '/service',
    component: Layout,
    redirect: '/service/manage',
    meta: { title: '服务管理', icon: 'iconfont icon-zhinengshoubiao', code: 'serviceitemmanage' },
    children: [
      // {
      //   path: 'config',
      //   component: () => import('@/views/service/config/index'),
      //   name: 'Service Configuration',
      //   meta: { title: '服务配置', icon: 'iconfont icon-jiankangguanli', code: 'servicemanager_config' }
      // }
      {
        path: 'list',
        component: () => import('@/views/service/list/index'),
        name: 'Service List',
        meta: { title: '服务信息', icon: 'iconfont icon-xinxi', code: 'service_item' }
      }
      // {
      //   path: 'info',
      //   component: () => import('@/views/device/list/info/index'),
      //   name: 'Device Info',
      //   hidden: true,
      //   meta: { title: '设备详情', icon: 'home', noCache: true, activeMenu: '/device/list' }
      // }
    ]
  },
  {
    path: '/log',
    component: Layout,
    redirect: '/log/manage',
    meta: { title: '日志管理', icon: 'iconfont icon-zhinengshoubiao', code: 'systemmonitor' },
    children: [
      // {
      //   path: 'config',
      //   component: () => import('@/views/service/config/index'),
      //   name: 'Service Configuration',
      //   meta: { title: '服务配置', icon: 'iconfont icon-jiankangguanli', code: 'servicemanager_config' }
      // }
      {
        path: 'list',
        component: () => import('@/views/log/index'),
        name: 'Log List',
        meta: { title: '日志信息', icon: 'iconfont icon-xinxi', code: 'system_log' }
      }
      // {
      //   path: 'info',
      //   component: () => import('@/views/device/list/info/index'),
      //   name: 'Device Info',
      //   hidden: true,
      //   meta: { title: '设备详情', icon: 'home', noCache: true, activeMenu: '/device/list' }
      // }
    ]
  },
  {

    path: '/health',
    component: Layout,
    redirect: '/health/record',
    meta: { title: '健康管理', icon: 'iconfont icon-jiankangguanli1', code: 'healthmanager' },
    children: [
      {
        path: 'record',
        component: () => import('@/views/health/record/index'),
        name: 'Health Monitoring',
        meta: { title: '监测记录', icon: 'iconfont icon-tuisong-tuisongpinlv-01', code: 'healthmanager_check' }
      },
      {
        path: 'record/info',
        component: () => import('@/views/health/record/info/index'),
        name: 'Record Info',
        hidden: true,
        meta: { title: '记录详情', icon: 'home', noCache: true, activeMenu: '/health/record', code: 'healthmanager_check' }
      },
      {
        path: 'disease',
        component: () => import('@/views/health/disease/index'),
        name: 'Health Disease',
        meta: { title: '疾病数据', icon: 'iconfont icon-jiankangguanli2', code: 'healthmanager_disease' }
      },
      {
        path: 'news',
        component: () => import('@/views/health/news/index'),
        name: 'Health News',
        meta: { title: '健康资讯', icon: 'iconfont icon-jiankangzixun', code: 'healthmanager_info' }
      },
      {
        path: 'news/save',
        component: () => import('@/views/health/news/save/index'),
        name: 'News Save',
        hidden: true,
        meta: { title: '健康资讯保存', icon: 'home', noCache: true, activeMenu: '/health/news', code: 'healthmanager_info' }
      }
    ]
  },
  {
    path: '/alarm',
    component: Layout,
    redirect: '/alarm/health',
    meta: { title: '预警管理', icon: 'iconfont icon-yujing', code: 'warnhmanager' },
    children: [
      {
        path: 'statistics',
        component: () => import('@/views/alarm/statistics/index'),
        name: 'Alarm Statistics',
        meta: { title: '预警统计', icon: 'iconfont icon-zaixianjiance', code: 'warnhmanager_tot' }
      },
      {
        path: 'health',
        component: () => import('@/views/alarm/health/index'),
        name: 'Alarm Health',
        meta: { title: '健康预警', icon: 'iconfont icon-jiankangguanli4', code: 'healthmanager_check' }
      },
      {
        path: 'fall',
        component: () => import('@/views/alarm/fall/index'),
        name: 'Alarm Fall',
        meta: { title: '跌倒报警', icon: 'iconfont icon-iconhuizong_huaban1fuben23', code: 'warnhmanager_feil' }
      },
      {
        path: 'sos',
        component: () => import('@/views/alarm/sos/index'),
        name: 'Alarm SOS',
        meta: { title: 'SOS求助', icon: 'iconfont icon-iconhuizong_huaban1fuben22', code: 'warnhmanager_sos' }
      },
      {
        path: 'fence',
        component: () => import('@/views/alarm/fence/index'),
        name: 'Alarm Fence',
        meta: { title: '围栏预警', icon: 'iconfont icon-dianziweilan', code: 'warnhmanager_fence' }
      },
      {
        path: 'healthInfo',
        component: () => import('@/views/alarm/health/info/index'),
        name: 'Alarm Health Info',
        hidden: true,
        meta: { title: '健康预警详情', icon: 'dashboard', activeMenu: '/alarm/health', code: 'healthmanager_check' }
      },
      {
        path: 'fallInfo',
        component: () => import('@/views/alarm/fall/info/index'),
        name: 'Alarm Fall Info',
        hidden: true,
        meta: { title: '跌倒预警详情', icon: 'dashboard', activeMenu: '/alarm/fall', code: 'warnhmanager_feil' }
      },
      {
        path: 'sosInfo',
        component: () => import('@/views/alarm/sos/info/index'),
        name: 'Alarm SOS Info',
        hidden: true,
        meta: { title: 'SOS求助详情', icon: 'dashboard', activeMenu: '/alarm/sos', code: 'warnhmanager_sos' }
      },
      {
        path: 'fenceInfo',
        component: () => import('@/views/alarm/fence/info/index'),
        name: 'Alarm Fence Info',
        hidden: true,
        meta: { title: '围栏预警详情', icon: 'dashboard', activeMenu: '/alarm/fence', code: 'warnhmanager_fence' }
      },
      {
        path: 'push',
        component: () => import('@/views/alarm/push/index'),
        name: 'Alarm Push',
        meta: { title: '报警推送', icon: 'iconfont icon-baojingtuisongshezhi', activeMenu: '/alarm/push', code: 'warnhmanager_send' }
      },
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
