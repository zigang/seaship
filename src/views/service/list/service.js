import request from '@/utils/request-management'

/**
 * 服务
 */
export default class Service {
  /**
   * 服务列表
   *
   * @param data
   * @returns {*}
   */
  static list = data => request.get('/api/manager/service/list', data)
  /**
   * 添加服务
   *
   * @param data
   * @returns {*}
   */
  static add = data => request.post('/api/manager/service/add', data)
  /**
   * 删除服务
   *
   * @param data
   * @returns {*}
   */
  static delete = data => request.delete('/api/manager/service/delete?id='+ data.id)
  /**
   * 编辑服务
   *
   * @param data
   * @returns {*}
   */
    static edit = data => request.put('/api/manager/service/edit',data)

}
