/**
 * ECharts 公共配置
 */

export default class EChartsConfig {
  grid = {
    left: 40,
    right: 30,
    top: 20,
    bottom: 30
  }

  xAxis = {
    axisLabel: {
      color: '#666666'
    },
    splitLine: {
      show: false
    },
    axisLine: {
      lineStyle: {
        color: '#C8C8C8'
      }
    },
    axisTick: {
      show: false
    }
  }

  yAxis = {
    type: 'value',
    splitLine: {
      show: false
    },
    axisLine: {
      show: false
    },
    axisTick: {
      show: false
    },
    axisLabel: {
      color: '#666666'
    }
  }
}
