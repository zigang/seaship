import EChartsConfig from '../index'
import { minutesToTime } from '@/utils/date'

export default class DailyOption extends EChartsConfig {
  constructor() {
    super()

    this.tooltip = {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      },
      formatter: function(params) {
        return minutesToTime(params[0].axisValue) + '<br/>' +
          '血氧 : ' + params[0].value[1]
      }
    }

    this.grid.left = 50
    this.grid.right = 20
    this.grid.top = 20
    this.grid.bottom = 30

    this.xAxis.type = 'value'
    this.xAxis.min = 0
    this.xAxis.max = 1440
    this.xAxis.interval = 240
    this.xAxis.axisLabel.formatter = function(value) {
      return value / 60 + ':00'
    }
    this.xAxis.boundaryGap = false
    this.yAxis.splitLine.show = true
    // this.yAxis.splitLine.lineStyle = {
    //   type: 'dashed'
    // }
    this.yAxis.axisLabel.formatter = function(value) {
      return value + '%'
    }
    this.yAxis.min = 80
    this.yAxis.max = 100
    this.yAxis.interval = 5
    this.yAxis.show = true
    this.yAxis.axisLine = {
      show: true
    }

    this.series = [
      {
        symbolSize: 11,
        color: '#11A811',
        data: [],
        type: 'scatter'
      }
    ]
  }
}
