import EChartsConfig from '../index'

export default class DailyOption extends EChartsConfig {
  constructor() {
    super()
    // grid
    this.grid.left = 40
    this.grid.right = 30
    this.grid.top = 20

    // xAxis
    this.xAxis.type = 'value'
    this.xAxis.min = 0
    this.xAxis.max = 1440
    this.xAxis.interval = 240
    this.xAxis.axisLabel.formatter = function(value) {
      return value / 60 + ':00'
    }

    // yAxis
    this.yAxis.min = 0
    this.yAxis.max = 1
    this.yAxis.interval = 1
    this.yAxis.show = false

    // series
    this.series = [
      {
        data: [],
        type: 'bar',
        barWidth: '100%',
        itemStyle: {
          color: (params) => (params.value[1] > 1 ? '#8B74E4' : '#64B1F1')
        },
        symbol: 'none'
      }
    ]
  }
}
