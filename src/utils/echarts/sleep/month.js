import EChartsConfig from '../index'

export default class MonthlyOption extends EChartsConfig {
  constructor() {
    super()

    this.tooltip = {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      },
      formatter: function(params) {
        return params[1].name + '<br/>' +
          params[1].seriesName + ' : ' + (params[0].value[1] + params[1].value[1]) + '<br/>' +
          params[0].seriesName + ' : ' + (params[0].value[1])
      }
    }

    // grid
    this.grid.left = 20
    this.grid.right = 20
    this.grid.top = 30

    // xAxis
    this.xAxis.type = 'category'
    this.xAxis.min = 1
    this.xAxis.max = 31
    this.xAxis.interval = 1
    // this.xAxis.axisLabel.formatter = function(value) {
    //   return value
    // }

    // yAxis
    this.yAxis.show = false

    // series
    this.series = [
      {
        type: 'bar',
        stack: 'sleep',
        itemStyle: {
          color: '#8B74E4'
        },
        data: []
      },
      {
        type: 'bar',
        stack: 'sleep',
        itemStyle: {
          color: '#3C8EF1'
        },
        data: []
      }
    ]
  }
}
