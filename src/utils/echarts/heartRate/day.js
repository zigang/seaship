import EChartsConfig from '../index'
import { minutesToTime } from '@/utils/date'

export default class DailyOption extends EChartsConfig {
  constructor() {
    super()
    this.tooltip = {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      },
      formatter: function(params) {
        return minutesToTime(params[0].axisValue) + '<br/>' +
          '心率 : ' + params[0].value[1]
      }
    }

    // grid
    this.grid.left = 40
    this.grid.right = 30
    this.grid.top = 20

    // xAxis
    this.xAxis.type = 'value'
    this.xAxis.min = 0
    this.xAxis.max = 1440
    this.xAxis.interval = 240
    this.xAxis.axisLabel.formatter = function(value) {
      return value / 60 + ':00'
    }

    // yAxis
    // this.yAxis.min = 0
    // this.yAxis.max = 165
    // this.yAxis.interval = 55
    this.yAxis = {
      min:0,
      max:165,
      interval:55,
      show: true,
      axisLine: {
        show: true,
        // lineStyle: {
        //   color: '#666'
        // }
      }
      // splitLine:{
      //   lineStyle: {
      //     color:'#666'
      //   }
      // }
    }

    // series
    this.series = [
      {
        data: [],
        // barWidth: 20,
        type: 'line',
        color: '#FF0303',
        symbol: 'none'
      }
    ]
  }
}
