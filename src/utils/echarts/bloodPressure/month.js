import EChartsConfig from '../index'

export default class MonthlyOption extends EChartsConfig {
  constructor() {
    super()
    this.tooltip = {
      trigger: 'axis'
    }

    // grid
    this.grid.left = 50
    this.grid.right = 20
    this.grid.top = 30

    // xAxis
    this.xAxis.type = 'category'
    this.xAxis.min = 1
    this.xAxis.max = 31
    this.xAxis.interval = 1
    // this.xAxis.axisLabel.formatter = function(value) {
    //   return value
    // }

    // yAxis
    // this.yAxis.show = false
    this.yAxis = {
      show: true,
      axisLine: {
        show: true
      }
    }

    // series
    this.series = [
      {
        name: '最小收缩压',
        type: 'bar',
        stack: 'Syst',
        itemStyle: {
          borderColor: 'transparent',
          color: 'transparent'
        },
        emphasis: {
          itemStyle: {
            borderColor: 'transparent',
            color: 'transparent'
          }
        },
        data: []
      },
      {
        name: '最大收缩压',
        type: 'bar',
        stack: 'Syst',
        color: '#00CCFF',
        label: {
          show: true,
          position: 'inside',
          fontSize:10,
          color:'#6e7079'
        },
        data: []
      },
      {
        name: '最小舒张压',
        type: 'bar',
        stack: 'Diast',
        itemStyle: {
          borderColor: 'transparent',
          color: 'transparent'
        },
        emphasis: {
          itemStyle: {
            borderColor: 'transparent',
            color: 'transparent'
          }
        },
        data: []
      },
      {
        name: '最大舒张压',
        type: 'bar',
        stack: 'Diast',
        color: '#758bff',
        label: {
          show: true,
          position: 'inside',
          fontSize:10,
          color:'#6e7079'
        },
        data: []
      }
    ]
  }
}
