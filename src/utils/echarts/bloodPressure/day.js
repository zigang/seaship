import EChartsConfig from '../index'
import { minutesToTime } from '@/utils/date'

export default class DailyOption extends EChartsConfig {
  constructor() {
    super()
    this.tooltip = {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      },
      formatter: function(params) {
        try {
          if (params.length > 2) {
            return minutesToTime(params[0].axisValue) + '<br/>' +
              params[0].seriesName + ' : ' + params[0].value[1] + '<br/>' +
              params[2].seriesName + ' : ' + params[2].value[1]
          } else {
            return minutesToTime(params[0].axisValue) + '<br/>' +
              params[0].seriesName + ' : ' + params[0].value[1] + '<br/>' +
              params[1].seriesName + ' : ' + params[1].value[1]
          }
        } catch (error) {
          console.log('error',error)
        }
        
      }
    }

    this.legend = {
      top: 10,
      right: 10,
      data: ['收缩压', '舒张压']
    }

    this.grid.left = 40
    this.grid.right = 20
    this.grid.top = 45
    this.grid.bottom = 30

    this.xAxis.type = 'value'
    this.xAxis.min = 0
    this.xAxis.max = 1440
    this.xAxis.interval = 240
    this.xAxis.axisLabel.formatter = function(value) {
      return value / 60 + ':00'
    }
    this.xAxis.boundaryGap = false
    this.yAxis = {
      show: true,
      axisLine: {
        show: true
      }
    }

    this.series = [
      {
        name: '收缩压',
        type: 'line',
        color: '#00CCFF',
        symbol: 'none',
        data: []
      },
      {
        name: '舒张压',
        type: 'line',
        color: '#758bff',
        symbol: 'none',
        data: []
      }
    ]
  }
}
