import EChartsConfig from '../index'

export default class YearlyOption extends EChartsConfig {
  constructor() {
    super()
    this.tooltip = {
      trigger: 'axis'
    }

    this.legend = {
      top: 10,
      right: 10,
      data: ['收缩压', '舒张压']
    }

    this.grid.left = 40
    this.grid.right = 20
    this.grid.top = 45
    this.grid.bottom = 30

    this.xAxis.type = 'category'
    // this.xAxis.min = 0
    // this.xAxis.max = 12
    // this.xAxis.interval = 1
    this.xAxis.data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    this.yAxis = {
      show: true,
      axisLine: {
        show: true
      }
    }
    this.series = [
      {
        name: '最小收缩压',
        type: 'bar',
        stack: 'Syst',
        itemStyle: {
          borderColor: 'transparent',
          color: 'transparent'
        },
        emphasis: {
          itemStyle: {
            borderColor: 'transparent',
            color: 'transparent'
          }
        },
        data: []
      },
      {
        name: '最大收缩压',
        type: 'bar',
        stack: 'Syst',
        color: '#00CCFF',
        label: {
          show: true,
          position: 'inside',
          fontSize:10
        },
        data: []
      },
      {
        name: '最小舒张压',
        type: 'bar',
        stack: 'Diast',
        itemStyle: {
          borderColor: 'transparent',
          color: 'transparent'
        },
        emphasis: {
          itemStyle: {
            borderColor: 'transparent',
            color: 'transparent'
          }
        },
        data: []
      },
      {
        name: '最大舒张压',
        type: 'bar',
        stack: 'Diast',
        color: '#758bff',
        label: {
          show: true,
          position: 'inside',
          fontSize:10
        },
        data: []
      }
    ]
  }
}
