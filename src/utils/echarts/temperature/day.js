import EChartsConfig from '../index'
import { minutesToTime } from '@/utils/date'

export default class DailyOption extends EChartsConfig {
  constructor() {
    super()
    this.tooltip = {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      },
      formatter: function(params) {
        console.log('zzz', params)
        return minutesToTime(params[0].axisValue) + '<br/>' +
          '体温 : ' + params[0].value[1]
      }
    }

    // grid
    this.grid.left = 50
    this.grid.right = 30
    this.grid.top = 20

    // xAxis
    this.xAxis.type = 'value'
    this.xAxis.min = 0
    this.xAxis.max = 1440
    this.xAxis.interval = 240
    this.xAxis.axisLabel.formatter = function(value) {
      return value / 60 + ':00'
    }

    // yAxis
    this.yAxis = {
      show: true,
      axisLine: {
        show: true
      }
    }
    this.yAxis.min = 30
    this.yAxis.max = 40
    this.yAxis.interval = 2
    // series
    this.series = [
      {
        data: [],
        symbolSize: 8,
	      type: 'scatter',
        color: '#57e1f7'
      }
    ]
  }
}
