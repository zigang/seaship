import EChartsConfig from '../index'

export default class MonthlyOption extends EChartsConfig {
  constructor() {
    super()

    this.tooltip = {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      },
      formatter: function(params) {
        return params[1].name + '<br/>' +
          params[1].seriesName + ' : ' + (params[0].value[1] + params[1].value[1]) + '<br/>' +
          params[0].seriesName + ' : ' + (params[0].value[1])
      }
    }

    // grid
    this.grid.left = 50
    this.grid.right = 20
    this.grid.top = 30

    // xAxis
    this.xAxis.type = 'category'
    this.xAxis.min = 1
    this.xAxis.max = 31
    this.xAxis.interval = 1
    // this.xAxis.axisLabel.formatter = function(value) {
    //   return value
    // }

    // yAxis
    
    this.yAxis = {
      show: true,
      axisLine: {
        show: true
      }
    }
    this.yAxis.min = 30
    this.yAxis.max = 40

    // series
    this.series = [
      {
        name: '最小温度',
        type: 'bar',
        stack: 'Total',
        // barWidth: 20,
        itemStyle: {
          borderColor: 'transparent',
          color: 'transparent'
        },
        emphasis: {
          itemStyle: {
            borderColor: 'transparent',
            color: 'transparent'
          }
        },
        data: []
      },
      {
        name: '最大温度',
        type: 'bar',
        stack: 'Total',
        color: '#57e1f7',
        label: {
          show: true,
          position: 'inside'
        },
        // barWidth: 20,
        data: []
      }
    ]
  }
}
