import EChartsConfig from '../index'

export default class MonthlyOption extends EChartsConfig {
  constructor() {
    super()

    // grid
    this.grid.left = 50
    this.grid.right = 20
    this.grid.top = 30

    // xAxis
    this.xAxis.type = 'value'
    this.xAxis.min = 0
    this.xAxis.max = 31
    this.xAxis.interval = 1
    this.xAxis.axisLabel.formatter = function(value) {
      return value
    }

    // yAxis
    this.yAxis = {
      show: true,
      axisLine: {
        show: true
      }
    }

    // series
    this.series = [
      {
        type: 'bar',
        // barWidth: 10,
        color: '#227CFF',
        label: {
          show: true,
          position: 'top',
          fontSize:10
        },
        data: []
      }
    ]
  }
}
