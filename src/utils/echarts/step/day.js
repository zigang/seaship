import EChartsConfig from '../index'

export default class DailyOption extends EChartsConfig {
  constructor() {
    super()

    this.tooltip = {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      },
      formatter: function(params) {
        console.log('zzz', params)
        return '' + params[0].axisValue + ':00' + '<br/>' +
          '步数 : ' + params[0].value[1]
      }
    }

    // grid
    this.grid.left = 50
    this.grid.right = 20
    this.grid.top = 30

    // xAxis
    this.xAxis.type = 'value'
    this.xAxis.min = 0
    this.xAxis.max = 24
    this.xAxis.interval = 4
    this.xAxis.axisLabel.formatter = function(value) {
      return value + ':00'
    }

    // yAxis
    this.yAxis = {
      show: true,
      axisLine: {
        show: true
      }
    }

    // series
    this.series = [
      {
        type: 'bar',
        // barWidth: 20,
        color: '#227CFF',
        label: {
          show: true,
          position: 'top',
          fontSize:10
        },
        itemStyle: {
          borderRadius: [6, 6, 0, 0]
        },
        data: []
      }
    ]
  }
}
