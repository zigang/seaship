import EChartsConfig from '../index'

export default class YearlyOption extends EChartsConfig {
  constructor() {
    super()

    // grid
    this.grid.left = 50
    this.grid.right = 20
    this.grid.top = 30

    // xAxis
    this.xAxis.type = 'category'
    this.xAxis.data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

    // yAxis
    this.yAxis = {
      show: true,
      axisLine: {
        show: true
      }
    }

    // series
    this.series = [
      {
        type: 'bar',
        // barWidth: 20,
        color: '#227CFF',
        label: {
          show: true,
          position: 'top',
          fontSize:10
        },
        itemStyle: {
          borderRadius: [6, 6, 0, 0]
        },
        data: []
      }
    ]
  }
}
