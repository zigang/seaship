import Vue from 'vue'
import AMapLoader from '@amap/amap-jsapi-loader'

export const initAMap = new Promise((resolve, reject) => {
  window._AMapSecurityConfig = { securityJsCode: 'c0ee6c0be6cb3d5b253929895afc848b' };
  AMapLoader.load({
    key: '2466935971de4511cb52bc8726e971d7', // key
    // plugins: ['Map.Geocoder'], // 插件
    version: '2.0'
  }).then((AMap) => {
    resolve(AMap)
    if (!Vue.prototype.$AMap) {
      Vue.prototype.$AMap = AMap
    }
    if (!window.AMap) {
      window.AMap = AMap
    }
  }).catch(e => {
    console.error(e)
    reject(e)
  })
})

export const install = (Vue) => {
  initAMap.then(AMap => {
    if (!Vue.prototype.$AMap) {
      Vue.prototype.$AMap = AMap
    }
    if (!window.AMap) {
      window.AMap = AMap
    }
  })
}

export default {
  install
}
