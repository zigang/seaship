import moment from 'moment'

export function getMinutesOfDay(params) {
  const date = new Date(params)
  return date.getHours() * 60 + date.getMinutes()
}

export function minutesToTime(params) {
  const date = new Date()
  date.setHours(parseInt(params / 60))
  date.setMinutes(params % 60)
  return formatter(date, 'HH:mm')
}

export function getHoursOfDay(params) {
  const date = new Date(params)
  return date.getHours() + 1
}

export function getDayOfMonth(params) {
  const date = new Date(params)
  return date.getDate()
}

export function getMonthOfYear(params) {
  const date = new Date(params)
  return date.getMonth()
}

export function getDays(params) {
  const date = new Date(params)
  return new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate()
}

export function formatter(date, fmt = 'YYYY-MM-DD HH:mm:ss') {
  if (date) {
    const mom = moment(date)
    if (mom.isValid()) {
      return mom.format(fmt)
    }
  }
  return null
}
