export function arrayOf(length) {
  const result = new Array(length)
  for (let i = 0; i < length; i++) {
    result[i] = i
  }
  return result
}
