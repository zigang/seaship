import * as QiNiu from 'qiniu-js'
import { generateUniqueName } from './generator'
import api from '@/api/index'
import { showLoading, hideLoading } from '@/utils/loading'

export function upload(file) {
  showLoading()
  return new Promise((resolve, reject) => {
    // const key = 'file/' + (file.uid || generateUniqueName(file))
    const key = 'file/' + generateUniqueName(file)
    api.file.getQiNiuToken({ fileName: key })
      .then(res => {
        const config = {
          useCdnDomain: true, // 表示是否使用 cdn 加速域名，为布尔值，true 表示使用，默认为 false。
          region: QiNiu.region.z2 // 根据具体提示修改上传地区,当为 null 或 undefined 时，自动分析上传域名区域
        }
        const putExtra = {
          fname: file.name, // 文件原文件名
          params: {}, // 用来放置自定义变量
          mimeType: null // 用来限制上传文件类型，为 null 时表示不对文件类型限制；限制类型放到数组里： ["image/png", "image/jpeg", "image/gif"]
        }
        const observable = QiNiu.upload(
          file,
          key,
          res.data,
          putExtra,
          config
        )
        observable.subscribe({
          next: (result) => {
            // 主要用来展示进度
            console.log(result)
          },
          error: (errResult) => {
            // 失败报错信息
            hideLoading()
            console.log(errResult)
            reject(errResult)
          },
          complete: (result) => {
            hideLoading()
            // 接收成功后返回的信息
            if (result.key) {
              resolve('https://img.greatech.com.cn/' + result.key)
            } else {
              reject()
            }
          }
        })
      }).catch(e => {
        hideLoading()
        reject(e)
      })
  })
}
