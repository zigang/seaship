export function uuid() {
  const temp_url = URL.createObjectURL(new Blob())
  const uuid = temp_url.toString()
  URL.revokeObjectURL(temp_url)
  return uuid.split(/[:\/]/g).pop().replaceAll('-', '')
}

export function generateUniqueName(file) {
  const extensionName = getExtensionName(file)
  return uuid() + (extensionName ? ('.' + extensionName) : '')
}

export function getExtensionName(file) {
  const fileName = file.name
  if (fileName.lastIndexOf('.') !== -1 && fileName.lastIndexOf('.') !== 0) {
    return fileName.substring(fileName.lastIndexOf('.') + 1)
  }
  return ''
}
