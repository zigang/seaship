import Vue from 'vue'
import { formatter } from "@/utils/date";

Vue.filter('formatDate',function(date,fmt){
    return formatter(date,fmt);
})
