import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'
import { showLoading, hideLoading } from '@/utils/loading'

import JsonBig from 'json-bigint'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_ACCOUNT_BASE_URL, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 10000 // request timeout
})

// request interceptor
service.interceptors.request.use(config => {
  // do something before request is sent
  showLoading()

  if (store.getters.token) {
    // let each request carry token
    // ['X-Token'] is a custom headers key
    // please modify it according to the actual situation
    config.headers['token'] = getToken()
  }
  config.transformResponse = [(data) => {
    try {
      return JsonBig.parse(data)
    } catch (e) {
      console.log('error', e)
    }
    return data
  }]
  return config
}, error => {
  // do something with request error
  console.log(error) // for debug
  return Promise.reject(error)
})

// response interceptor
service.interceptors.response.use(/**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */response => {
    hideLoading()

    const res = response.data

    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 0) {
      Message.error(res.message || 'Error')

      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.code === 401) {
        // to re-login
        MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
          confirmButtonText: 'Re-Login', cancelButtonText: 'Cancel', type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  }, error => {
    hideLoading()
    console.log('err' + error) // for debug

    let message = error.message
    if (error.message.startsWith('timeout of ') && error.message.endsWith('ms exceeded')) {
      message = '请求超时，请稍后再试'
    }
    Message.error(message)

    return Promise.reject(error)
  })

const get = (url, data) => service({
  url, method: 'get', params: data
})

const post = (url, data) => service({
  url, method: 'post', header: {
    'Content-Type': 'application/json;charset=UTF-8'
  }, data
})

const put = (url, data) => service({
  url, method: 'put', data
})

const DELETE = (url, data) => service({
  url, method: 'delete', data
})

export default {
  namespaced: true, get, post, put, delete: DELETE
}
